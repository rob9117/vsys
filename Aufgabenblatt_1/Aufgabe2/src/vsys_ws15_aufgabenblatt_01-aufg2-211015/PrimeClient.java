import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.sun.org.apache.xpath.internal.operations.Bool;
import rm.requestResponse.*;

import javax.sound.midi.SysexMessage;

public class PrimeClient extends Thread {
    private static final String HOSTNAME = "localhost";
    private static final int PORT = 1234;
    private static final long INITIAL_VALUE = (long) 1e17;
    private static final long COUNT = 20;
    private static final String CLIENT_NAME = PrimeClient.class.getName();
    private static final String SECOND_REQUEST_MODE = "nicht blockierend";

    private Component communication;
    String hostname, requestMode;
    int port;
    long initialValue, count;
    boolean synchronizedBoolean = true;
    boolean blockBoolean = false;
    private static Object syncHelperObject = new Object();

    public PrimeClient(String hostname, int port, boolean synchronizedBoolean, boolean blockBoolean, long
            initialValue,
                       long
                               count) {

        this.hostname = hostname;
        this.port = port;
        this.initialValue = initialValue;
        this.count = count;
        this.synchronizedBoolean = synchronizedBoolean;
        this.blockBoolean = blockBoolean;
    }

    public void run() {
        communication = new Component();
        for (long i = initialValue; i < initialValue + count; i++)
            try {
                processNumber(i);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
    }

    public void processNumber(long value) throws IOException, ClassNotFoundException {
        boolean inProgress = false;
        String progressString = ".";

        if (!synchronizedBoolean) {
            syncHelperObject = this;
        } else {
            System.out.println(value + ":");
        }


        synchronized (syncHelperObject) {
            communication.send(new Message(hostname, port, new Long(value)), false);
            Message message = null;

            if(blockBoolean) {
                while (message == null) {
                    progressString += ".";
                    System.out.print(progressString);
                }
            }

            Boolean isPrime = (Boolean) message.getContent();
            System.out.println(value + ": " + (isPrime.booleanValue() ? "prime" : "not prime"));
        }

    }


    public static void main(String args[]) throws IOException, ClassNotFoundException {
        String hostname = HOSTNAME;
        int port = PORT;
        String requestMode = "SYNCHRONIZED";
        boolean synchronizedBoolean = true;
        boolean blockBoolean = false;
        long initialValue = INITIAL_VALUE;
        long count = COUNT;
        String secondRequestMode = SECOND_REQUEST_MODE;

        boolean doExit = false;

        String input;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Welcome to " + CLIENT_NAME + "\n");

        while (!doExit) {
            //hostname
            System.out.print("Server hostname [" + hostname + "] > ");
            input = reader.readLine();
            if (!input.equals("")) hostname = input;

            //port
            System.out.print("Server port [" + port + "] > ");
            input = reader.readLine();
            if (!input.equals("")) port = Integer.parseInt(input);

            //request mode
            System.out.print("Request mode [" + requestMode + "] > ");
            input = reader.readLine();
            if (input.equals("1")) {
                synchronizedBoolean = true;
                requestMode = "SYNCHRONIZED";
                System.out.println("you choose SYNCHRONIZED!");
            } else if (input.equals("0")) {
                System.out.println("you choose UNSYNCHRONIZED!");
                synchronizedBoolean = false;
                requestMode = "UNSYNCHRONIZED";
            }
            if (!synchronizedBoolean) {
                //second request mode
                System.out.println("Modus [" + secondRequestMode + "] >");
                input = reader.readLine();
                System.out.println(input);
                switch (input) {
                    case "0":
                        secondRequestMode = "blockierend";
                        blockBoolean = true;
                        System.out.println("you choosed blockierend");
                        break;
                    case "1":
                        secondRequestMode = "nicht blockierend";
                        blockBoolean = false;
                        System.out.println("you choosed nicht blockierend");
                        break;
                    default:
                        blockBoolean = true;
                        secondRequestMode = "blockierend";
                        break;
                }
            }


            //initial value
            System.out.print("Prime search initial value [" + initialValue + "] > ");
            input = reader.readLine();
            if (!input.equals("")) initialValue = Integer.parseInt(input);

            //prime search count
            System.out.print("Prime search count [" + count + "] > ");
            input = reader.readLine();
            if (!input.equals("")) count = Integer.parseInt(input);

            //exit
            System.out.println("Exit [n]> ");
            input = reader.readLine();
            if (input.equals("y") || input.equals("j")) doExit = true;

            //run thread
            //new Thread().run();
            new PrimeClient(hostname, port, synchronizedBoolean, blockBoolean, initialValue, count).run();

        }
    }
}

