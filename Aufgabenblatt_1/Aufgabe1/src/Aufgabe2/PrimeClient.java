package Aufgabe2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.omg.PortableServer.REQUEST_PROCESSING_POLICY_ID;
import rm.requestResponse.*;

public class PrimeClient extends Thread {
    private static final String HOSTNAME = "localhost";
    private static final int PORT = 1234;
    private static final long INITIAL_VALUE = (long) 1e17;
    private static final long COUNT = 20;
    private static final String CLIENT_NAME = PrimeClient.class.getName();
    private static final String REQUEST_MODE = "SYNCHRONIZED";
    private static Object syncHelperObject = new Object();

    private Component communication;
    String hostname;
    int port;
    long initialValue, count;
    String requestMode;

    public PrimeClient(String hostname, int port, String requestMode, long initialValue, long count) {
        this.hostname = hostname;
        this.port = port;
        this.initialValue = initialValue;
        this.count = count;
        this.requestMode = requestMode;
    }

    public void run() {
        communication = new Component();
        if (this.requestMode.equals("NOT SYNCHRONIZED")) {
            syncHelperObject = this;
        }
        for (long i = initialValue; i < initialValue + count; i++) {
            processNumber(i);
        }

    }

    public void processNumber(long value) {
        Boolean isPrime = null;

        synchronized (syncHelperObject) {
            try {
                communication.send(new Message(hostname, port, new Long(value)), false);
                isPrime = (Boolean) communication.receive(port, true, true).getContent();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.out.println(value + ": " + (isPrime.booleanValue() ? "prime" : "not prime"));
    }

    public static void main(String args[]) throws IOException, ClassNotFoundException {
        String hostname = HOSTNAME;
        int port = PORT;
        long initialValue = INITIAL_VALUE;
        long count = COUNT;
        String requestMode = REQUEST_MODE;

        boolean doExit = false;

        String input;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Welcome to " + CLIENT_NAME + "\n");

        while (!doExit) {
            System.out.print("Server hostname [" + hostname + "] > ");
            input = reader.readLine();
            if (!input.equals("")) hostname = input;

            System.out.print("Server port [" + port + "] > ");
            input = reader.readLine();
            if (!input.equals("")) port = Integer.parseInt(input);

            System.out.print("Request mode [" + requestMode + "] Enter 1 or 0 > ");
            input = reader.readLine();
            if (!input.equals("") && input.equals("1")) requestMode = "SYNCHRONIZED";
            if (!input.equals("") && input.equals("0")) requestMode = "NOT SYNCHRONIZED";


            System.out.print("Prime search initial value [" + initialValue + "] > ");
            input = reader.readLine();
            if (!input.equals("")) initialValue = Integer.parseInt(input);

            System.out.print("Prime search count [" + count + "] > ");
            input = reader.readLine();
            if (!input.equals("")) count = Integer.parseInt(input);

            new PrimeClient(hostname, port, requestMode, initialValue, count).start(); //CREATE THREAD

            System.out.println("Exit [n]> ");
            input = reader.readLine();
            if (input.equals("y") || input.equals("j")) doExit = true;
        }
    }
}

