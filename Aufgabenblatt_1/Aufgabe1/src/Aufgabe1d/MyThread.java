package Aufgabe1d;

import java.util.Random;

public class MyThread extends MyAccount implements Runnable{
    private static final int threadMax=10;
    private static int runCount=0;
    private static MyAccount myAccount = new MyAccount();
    public void run() {
        while(runCount++ <100){
            threadPrint(runCount);
        }
    }

    public void threadPrint(int runCount) {
        int value = myAccount.getValue();
        System.out.println(runCount + ": " + Thread.currentThread().getName() + ": " + randomIncrementDecrement(value, myAccount));
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String randomIncrementDecrement(int value, MyAccount myAccount) {
        Random random = new Random();
        int max = 20;
        int min = 1;
        int randomValue = random.nextInt(min + max + 1) + min;

        if(randomValue < 10) {
            myAccount.setValue(++value);
            return value + " + 1 = " + ++value;
        } else {
            myAccount.setValue(--value);
            return value + " - 1 = " + --value;

        }
    }

    public static void main(String args[]) {
        myAccount.setValue(1);
        for(int i=0; i<threadMax; i++) {
            new Thread(new MyThread()).start();
        }
    }
}

