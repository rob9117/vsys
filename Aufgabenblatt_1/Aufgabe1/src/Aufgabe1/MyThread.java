package Aufgabe1;

public class MyThread extends Thread {
    private static final int threadMax=10;
    private static int runCount=0;
    private static Object object = new Object();

    public void run() {
        while(runCount++ <100){
            synchronized (object){
                threadPrint(runCount);
            }
        }
    }

    public synchronized void threadPrint(int runCount) {
            System.out.println(runCount + ": " + Thread.currentThread().getName());
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    public static void main(String args[]) {
        for(int i=0; i<threadMax; i++) {
            new MyThread().start();
        }
    }
}

