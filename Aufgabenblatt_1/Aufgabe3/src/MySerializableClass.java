import java.io.Serializable;

public class MySerializableClass implements Serializable{
	private static final long serialVersionUID=1;
	private int id;
	private String string;
	// b) MyNonSerializableClass myNonClass; geht nicht da MyNonSerializableClass nicht Serializable implementiert, kommen Fehler
	// c) transient MyNonSerializableClass myNonClass; mit transient funktioniert es, aber Werte sind von myNonClass sind null, weil es nicht berücksichtigt wird
	
	MySerializableClass() {
		id=1234;
		// b) myNonClass = new MyNonSerializableClass();
	}
	
	public void set(String string) {
		this.string=string;
	}
	
	public String toString() {
		return "id: "+id+"; string: "+string; //+"; MyNonSerializableClass: "+myNonClass;
	}
} 
	