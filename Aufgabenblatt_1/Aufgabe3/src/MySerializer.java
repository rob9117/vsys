import java.io.*;

public class MySerializer {
	private MySerializableClass mySerializableClass;
	
	MySerializer(MySerializableClass serializableClass) {
		mySerializableClass=serializableClass;
	}
	
	private String readFilename() throws IOException {
		String filename;
		BufferedReader reader=new BufferedReader(new InputStreamReader(System.in )); 
		
		System.out.print("filename> ");
		filename=reader.readLine();
		
		return filename;
	}
	
	public void write(String text) throws IOException {
		mySerializableClass.set(text);
		String filename=readFilename();
		
		FileOutputStream fout = new FileOutputStream(filename);
		ObjectOutputStream oos = new ObjectOutputStream(fout);   
		
		oos.writeObject(mySerializableClass);
		oos.close();
		
		// Implementierung erforderlich
		// Serialisiere mySerializableClass in Datei
		
	}
	
	public String read() throws IOException, ClassNotFoundException {
		String filename=readFilename();

        FileInputStream fin = new FileInputStream(filename);
        ObjectInputStream ois = new ObjectInputStream(fin);
        mySerializableClass = (MySerializableClass) ois.readObject();
        ois.close();
		
		// Implementierung erforderlich
		// Serialisiere mySerializableClass von Datei
		
		return mySerializableClass.toString();
	}
} 
	