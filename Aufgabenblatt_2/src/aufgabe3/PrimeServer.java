package aufgabe3;

import java.io.IOException;
import java.util.logging.*;

import rm.requestResponse.*;

public class PrimeServer {
    private final static int PORT = 1234;
    private final static Logger LOGGER = Logger.getLogger(PrimeServer.class.getName());

    private Component communication;
    private int port = PORT;

    public PrimeServer(int port) {
        communication = new Component();
        if (port > 0) {
            this.port = port;
        }
    }

    private boolean primeService(long number) {
        for (long i = 2; i < Math.sqrt(number) + 1; i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }

    void setLogLevel(Level level) {
        for (Handler h : LOGGER.getLogger("").getHandlers()) h.setLevel(level);
        LOGGER.setLevel(level);
        LOGGER.info("Log level set to " + level);
    }

    void listen() {
        LOGGER.info("Listening on port " + port);

        while (true) {

            LOGGER.finer("Receiving ...");
            try {
                Message request = communication.receive(port, true, false);

                new Thread(new Messanger(request)).start();
            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        int port = 0;

        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-port":
                    try {
                        port = Integer.parseInt(args[++i]);
                    } catch (NumberFormatException e) {
                        LOGGER.severe("port must be an integer, not " + args[i]);
                        System.exit(1);
                    }
                    break;
                default:
                    LOGGER.warning("Wrong parameter passed ... '" + args[i] + "'");
            }
        }

        new PrimeServer(port).listen();
    }

    private class Messanger implements Runnable {

        private Message message;

        public Messanger(Message message) {
            this.message = message;
        }

        @Override
        public final void run() {
            try {
                communication.send(
                        new Message("localhost", port,
                        new Boolean(primeService((Long)this.message.getContent()))),
                        this.message.getPort(),
                        true
                );
                LOGGER.fine("Message was sent");
            } catch (IOException e) {
                Logger.getLogger(PrimeServer.class.getName()).log(Level.SEVERE, null, e.getMessage());
            }
        }
    }
}
