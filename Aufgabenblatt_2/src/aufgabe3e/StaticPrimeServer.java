package aufgabe3e;
import rm.requestResponse.Component;
import rm.requestResponse.Message;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StaticPrimeServer {
    private final static Logger LOGGER = Logger.getLogger(StaticPrimeServer.class.getName());
    private final static int PORT = 1235;

    private Component communication;
    private int port = PORT;
    private Counter counter = new Counter();

    ExecutorService threadPool = Executors.newFixedThreadPool(3);

    private class Messanger implements Runnable {

        private Message message;

        public Messanger(Message message) {
            this.message = message;
            counter.incrementCount();
        }

        @Override
        public final void run() {
            try {
                LOGGER.fine(this.message.toString() + " received.");
                LOGGER.fine(threadPool.toString());
                LOGGER.finer("Sending ...");
                communication.send(new Message("localhost", port,
                                new Boolean(primeService((Long) this.message.getContent()))),
                        this.message.getPort(), true);
                LOGGER.fine("message sent.");
                counter.decrementCount();
            } catch (IOException ex) {
                Logger.getLogger(StaticPrimeServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    StaticPrimeServer(int port) {
        communication = new Component();
        if (port > 0)
            this.port = port;
        setLogLevel(Level.FINER);
    }

    private boolean primeService(long number) {
        for (long i = 2; i < Math.sqrt(number) + 1; i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }

    //InterruptedException verschluckt
    void setLogLevel(Level level) {
        for (Handler h : LOGGER.getLogger("").getHandlers()) h.setLevel(level);
        LOGGER.setLevel(level);
        LOGGER.info("Log level set to " + level);
    }

    void listen() throws InterruptedException{
        LOGGER.info("Listening on port " + port);
        counter.start();

        while (true) {
            LOGGER.finer("Receiving ...");
            try {
                Message request = communication.receive(port, true, false);

                Runnable thread = new Messanger(request);
                threadPool.execute(thread);
            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        int port = 0;

        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-port":
                    try {
                        port = Integer.parseInt(args[++i]);
                    } catch (NumberFormatException e) {
                        LOGGER.severe("port must be an integer, not " + args[i]);
                        System.exit(1);
                    }
                    break;
                default:
                    LOGGER.warning("Wrong parameter passed ... '" + args[i] + "'");
            }
        }

        try{
            new StaticPrimeServer(port).listen();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
