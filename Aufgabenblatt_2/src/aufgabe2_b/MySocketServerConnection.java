package aufgabe2_b;

import java.io.*;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MySocketServerConnection extends Thread {
	private Socket socket;
	private String fileName;
	private File requestedFile;
	private WriteReadHandler writer;

	
	public MySocketServerConnection(Socket socket, String fileName) throws IOException {
		this.socket = socket;
		this.fileName = fileName;
		writer = new WriteReadHandler(fileName);
	}
	
	public void run() {
		writer.write("Server: waiting for message ...");
		try {
			readFromRequest();
			renderView();
		}
		catch(Exception e) {
			writer.write(e.getMessage());
			e.printStackTrace();
		}
	}

	public void readFromRequest() {
		try {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String requestData = bufferedReader.readLine();
			writer.write("Request: " + requestData);
			String regex = ".*GET (.*) HTTP.*";
			Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE | Pattern.DOTALL);
			Matcher matcher = pattern.matcher(requestData);
			if(matcher.matches()) {
				fileName = matcher.group(1);
				if(fileName.equals("/")) {
					requestedFile = new File("src/aufgabe2_b/views/index.html");
				} else {
					requestedFile = new File("src/aufgabe2_b/views/" + fileName);
				}
				writer.write("Requsted file: " + requestedFile.getPath());
			}
		} catch (IOException e) {
			writer.write(e.getMessage());
			e.printStackTrace();
		}
	}

	public void renderView() {
		if(requestedFile.exists() && requestedFile.isFile()) {
			writer.write("Server reads: " + requestedFile.getPath());
			WriteReadHandler reader = new WriteReadHandler();
			String fileContent = reader.read(requestedFile);
			String httpHeader = constructHeader(fileContent.length(), "200 OK");
			try {
				socket.getOutputStream().write(httpHeader.getBytes());
				socket.getOutputStream().write(fileContent.getBytes());
				writer.write("Filename: " + requestedFile.getName() + " has been rendered");
			} catch (IOException e) {
				writer.write("Error: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	private static String constructHeader(final int stringLength, final String httpState) {
		String blank = "HTTP/1.1 %s\nContent-Length: %d\nContent-Type: text/html\nConnection: close\n\n";
		return String.format(blank, httpState, stringLength);
	}
}
