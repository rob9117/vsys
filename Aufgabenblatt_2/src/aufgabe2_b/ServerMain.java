package aufgabe2_b;

import java.io.IOException;

public class ServerMain {
	private static final int port=1234;
	private static final String fileName = "logfile.txt";
	private static MySocketServer server;

	public static void main(String args[]) {
		try {
			server=new MySocketServer(port, fileName);
			server.listen();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
}
