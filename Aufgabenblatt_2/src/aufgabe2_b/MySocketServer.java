package aufgabe2_b;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MySocketServer {
    private ServerSocket socket;
    private int port;
    private String fileName;

    public MySocketServer(int port, String fileName) throws IOException {
        this.port = port;
        this.fileName = fileName;
        socket = new ServerSocket(port);
    }

    public void listen() {
        WriteReadHandler writer = new WriteReadHandler(fileName);
        writer.write("listening on: http://localhost:" + port + "/");
        while (true) {
            try {
                Socket incomingConnection = socket.accept();
                MySocketServerConnection connection = new MySocketServerConnection(incomingConnection, fileName);
                connection.start();
                writer.write("Socket Address: listening on port: " + port);
                writer.write("Socket Address: " + incomingConnection.getInetAddress());
            } catch (IOException e) {
                writer.write(e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
