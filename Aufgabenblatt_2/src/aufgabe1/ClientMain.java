package aufgabe1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;

public class ClientMain {
	private static final int port=1234;
	private static final String hostname="localhost";

	public static void main(String args[]) {
		try {
			System.out.print("Client: Enter name> ");

			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String clientName = reader.readLine() + "-" + ManagementFactory.getRuntimeMXBean().getName();
			ClientThread clientThread = new ClientThread(hostname, port, clientName);
			clientThread.start();

			reader = new BufferedReader(new InputStreamReader(System.in));
			String input = reader.readLine();

			if(!input.isEmpty()) {
				System.out.println("Exiting...");
				clientThread.stopRequesting();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
