package aufgabe1;

public class ClientThread extends Thread {

    private String host;
    private int port;
    private String clientName;
    private boolean stop = false;
    private MySocketClient client;

    public ClientThread(String host, int port, String clientName) {
        this.host = host;
        this.port = port;
        this.clientName = clientName;
    }

    @Override
    public void run() {
        int i = 0;

        //between start and stop sleep random amount of seconds and send/receive data
        //then diconnect and reconect again until stop is true
        while (!stop) {
            try {
                client = new MySocketClient(host, port);
                Thread.sleep((long) (Math.random() * 3000));
                String receivedMsg = client.sendAndReceive(clientName + "-" + i++);
                System.out.println("received message from server: " + receivedMsg);
                client.disconnect();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }


    public void stopRequesting() {
        this.stop = true;
    }
}
