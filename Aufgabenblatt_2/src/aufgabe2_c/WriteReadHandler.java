package aufgabe2_c;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class WriteReadHandler {
    static BufferedWriter bufferedWriter;
    static PrintWriter printWriter;

    public WriteReadHandler(String fileName) {
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(fileName, true));
        } catch (IOException e) {
            write("Exception" + e.getMessage());
            e.printStackTrace();
        }
        printWriter = new PrintWriter(bufferedWriter, true);
    }

    public WriteReadHandler() {}

    public static void write(String text) {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = new Date();
            text = dateFormat.format(date) + " | " + text;
            printWriter.println(text);
            System.out.println(text);
            printWriter.flush();
    }

    public static String read(File file) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            return stringBuffer.toString();
        } catch (IOException e) {
            write("Exception: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
}
