package aufgabe2;

import java.io.*;
import java.net.*;
 
public class MySocketServerConnection extends Thread {
	private Socket socket;
	private ObjectOutputStream objectOutputStream;
	private String fileName;

	
	public MySocketServerConnection(Socket socket, String fileName) throws IOException {
		this.socket = socket;
		this.fileName = fileName;
		objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
	}
	
	public void run() {
		WriteReadHandler writer = new WriteReadHandler(fileName);
		writer.write("Server: waiting for message ...");
		try {
			objectOutputStream.writeObject("HTTP/1.1 200 OK\r\n\r\n<html><body><h1>Hallo Web-Welt</h1></body></html>");
			socket.close();
		}
		catch(Exception e) {
			writer.write(e.getMessage());
			e.printStackTrace();
		}
	}
}
