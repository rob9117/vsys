package aufgabe2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class WriteReadHandler {
    static BufferedWriter bufferedWriter;
    static PrintWriter printWriter;

    public WriteReadHandler(String fileName) {
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(fileName, true));
        } catch (IOException e) {
            write(e.getMessage());
            e.printStackTrace();
        }
        printWriter = new PrintWriter(bufferedWriter, true);
    }

    public static void write(String text) {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = new Date();
            text = dateFormat.format(date) + " | " + text;
            printWriter.println(text);
            System.out.println(text);
            printWriter.flush();
    }
}
